import { Ng2NotificationsPage } from './app.po';

describe('ng2-notifications App', () => {
  let page: Ng2NotificationsPage;

  beforeEach(() => {
    page = new Ng2NotificationsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
