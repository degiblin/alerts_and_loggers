# Alerts and Loggers

The goal of this project is to create a simple notification and logging service.

## Requirements

The Alerts require Bootstrap.

## Using

Import the NotificationsModule to use. 

The LoggerService has a log method that has the following signature:
    
    log(
        message: string, 
        level: number | null | undefined, 
        obj: any | null | undefined,
        prod: boolean | null | undefined
    ) 

The AlertService as an alert method that has the following signature:

    alert(
        heading: any | null | undefined,
        message: string, 
        level: number | null | undefined, 
        prod: boolean | null | undefined
    )

This will push the alert to the AlertComponent which you can place wherever you want as `<cmc-alert></cmc-alert>`.

## TODO

### Version 1.0.0

* LoggerService
    * ~~Basic Functionality~~
* AlertService
    * ~~Alert Observable~~
    * ~~Bootstrap Alerts~~
    * ~~Limit Number of Alerts visible at once~~

### Future
* LoggerService
    * More Features
* AlertService
    * Alert with Link
    * Alert with Modal