import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Alert } from './alert/alert.model';
import { environment } from '../../environments/environment';

@Injectable()
export class AlertService {
  // Observable string sources
  private alertSource = new Subject<Alert>();

  // Observable string streams
  alert$ = this.alertSource.asObservable();

  // Service message commands
  constructor() { }

  /**
   * 
   * 
   * @param {(any | null | undefined)} heading: Surrounded by <strong> at beginning of alert.
   * @param {string} message: the alert body.
   * @param {(number | null | undefined)} level: The severity level. 0: Success, 1: Info, 2: Warning, 3: Error.
   * @param {(boolean | null | undefined)} prod: If the message should be shown in production.
   * 
   * @memberOf AlertService
   */
  alert(heading: any | null | undefined, message: string, level: number | null | undefined, prod: boolean | null | undefined) {
    if (!(prod == false && environment.production == true)) {
      let result = new Alert();
      if (heading) {
        result.heading = (heading.constructor.name) ? heading.constructor.name : heading;
      }
      result.level = level;
      result.message = message;
      this.alertSource.next(result);
    }
  }

/**
 * 
 * Converts the severity level to a Bootstrap contextual code.
 * @param {number} level 
 * @returns {string} 
 * 
 * @memberOf AlertService
 */
  getLevel(level: number): string {
    switch (level) {
      case 0: {
        return "success";
      }
      case 1: {
        return "info";
      }
      case 2: {
        return "warning";
      }
      case 3: {
        return "danger";
      }
      default:
        return "default";
    }
  }

}
