import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './alert/alert.component';
import { LoggerService } from './logger.service';
import { AlertService } from './alert.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AlertComponent],
  providers: [LoggerService, AlertService],
  exports: [AlertComponent]
})
export class NotificationsModule { }
