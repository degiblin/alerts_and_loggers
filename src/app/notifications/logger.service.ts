import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class LoggerService {

  constructor() { }

  /**
   * 
   * 
   * @param {string} message: The body of the log.
   * @param {(number | null | undefined)} level: The severity level. 0: Success, 1: Info, 2: Warning, 3: Error
   * @param {(any | null | undefined)} obj: The object calling the logger or the name to be used. 
   * @param {(boolean | null | undefined)} prod: If the message should be shown in production.
   * 
   * @memberOf LoggerService
   */
  log(message: string, level: number | null | undefined, obj: any | null | undefined, prod: boolean | null | undefined) {
    if (!(prod == false && environment.production == true)) {
      let resultMessage = "%c";
      let cssString = "margin: 10px;";
      switch (level) {
        case 0: {
          resultMessage += "[Success]"
          cssString += "color: green;"
          break;
        }
        case 1: {
          resultMessage += "[Info]"
          cssString += "color: blue;"
          break;
        }
        case 2: {
          resultMessage += "[Warning]"
          cssString += "color: orange;"
          break;
        }
        case 3: {
          resultMessage += "[Error]"
          cssString += "color: red;"
          break;
        }
        default:
          break;
      }

      if (!prod) {
        resultMessage += "[Dev]"
        if (obj) {
          let name = (obj.constructor.name) ? obj.constructor.name : obj;
          resultMessage += `[${name}]`;
        }
      }
      resultMessage += " " + message;
      console.log(resultMessage, cssString);
    }
  }
}
