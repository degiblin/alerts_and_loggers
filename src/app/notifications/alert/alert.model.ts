export class Alert {
    heading: string;
    message: string;
    level: number;
}
