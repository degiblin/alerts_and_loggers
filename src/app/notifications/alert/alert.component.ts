import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert.service';
import { Alert } from './alert.model';

@Component({
  selector: 'cmc-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  notifications: Alert[] = [];
  limit = 5;
  receivedNotifications: Alert[] = [];

  constructor(private al: AlertService) {
    al.alert$.subscribe(alert => {
      this.receivedNotifications.unshift(alert);
    });
  }


  ngOnInit() {
    this.updateNotifications();
  }

  updateNotifications() {
    while (this.notifications.length < this.limit && this.receivedNotifications.length > 0) {
      this.notifications.push(this.receivedNotifications.pop());
    }
  }

  close(alert: Alert) {
    this.notifications.splice(this.notifications.indexOf(alert), 1);
    this.updateNotifications();
  }

}
