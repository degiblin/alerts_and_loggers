import { Component, OnInit } from '@angular/core';
import { LoggerService } from './notifications/logger.service';
import { AlertService } from './notifications/alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private logger: LoggerService, private al: AlertService) {
    this.logger.log("Hello Success!", 0, this, false);
    this.logger.log("Hello Info!", 1, this, true);
    this.logger.log("Hello Warning!", 2, this, true);
    this.logger.log("Hello Error!", 3, this, true);
  }
  title = 'app works!';

  ngOnInit(){
    for(let i = 0; i < 10; i++){
      this.al.alert("Alert", i.toString(), i % 4, false);
    }
  }
}
